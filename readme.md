#Installation
- [Install docker](https://docs.docker.com/install/)
- [Install docker-composer](https://docs.docker.com/compose/install/)
- run `docker-compose up -d`

#Usage
- follow http://localhost:51000/doc for REST API documentation
- run `docker-compose exec php bin/console` for CLI documentation
