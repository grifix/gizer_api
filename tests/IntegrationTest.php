<?php


namespace App\Tests;


use App\Application\Command\FetchGameResults\FetchGameResultsCommand;
use App\Infrastructure\CommandBus\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\RouterInterface;

final class IntegrationTest extends WebTestCase
{
    private RouterInterface $router;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
        $this->router = self::$container->get(RouterInterface::class);
    }

    public function testItGets(): void
    {
        $this->client->request('GET', $this->router->generate('game_results'));

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testItFetches():void {
        $commandBus = self::$container->get(CommandBusInterface::class);
        $commandBus->execute(new FetchGameResultsCommand());
    }
}
