<?php

namespace App\Infrastructure\QueryBus;

interface QueryBusInterface
{
    public function execute(object $query);
}
