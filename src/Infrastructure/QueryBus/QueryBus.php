<?php

declare(strict_types=1);


namespace App\Infrastructure\QueryBus;

use Symfony\Component\Messenger\MessageBusInterface;

final class QueryBus implements QueryBusInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function execute(object $query)
    {
        return $this->messageBus->dispatch($query);
    }
}
