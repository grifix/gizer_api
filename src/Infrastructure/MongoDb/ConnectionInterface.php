<?php

namespace App\Infrastructure\MongoDb;

use MongoDB\Model\BSONDocument;

interface ConnectionInterface
{
    public function startTransaction(): void;

    public function commitTransaction(): void;

    public function truncateCollection(string $database, $collection): void;

    public function bulkWrite(string $database, string $collection, array $documents): void;

    /**
     * @return BSONDocument[]
     */
    public function find(string $database, string $collection, array $sort = []): array;
}
