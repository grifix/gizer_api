<?php


namespace App\Infrastructure\MongoDb;


use MongoDB\Client;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Session;
use MongoDB\Model\BSONDocument;

final class Connection implements ConnectionInterface
{
    private Client $client;

    private Session $session;

    public function __construct(string $server, string $user, string $password)
    {
        $this->client = new Client(sprintf('mongodb://%s:%s@%s', $user, $password, $server));
        $this->session = $this->client->startSession();
    }

    public function startTransaction(): void
    {
        $this->session->startTransaction([]);
    }

    public function commitTransaction(): void
    {
        $this->session->commitTransaction();
    }

    public function find(string $database, string $collection, array $sort = []): array
    {
        return $this->client->selectCollection($database, $collection)->find([], ['sort' => $sort])->toArray();
    }

    public function truncateCollection(string $database, $collection): void
    {
        $this->client->selectCollection($database, $collection)->deleteMany([]);
    }

    public function bulkWrite(string $database, string $collection, array $documents): void
    {
        $bulk = new BulkWrite();
        foreach ($documents as $document) {
            $bulk->insert($document);
        }
        $this->client->getManager()->executeBulkWrite($database . '.' . $collection, $bulk);
    }

}
