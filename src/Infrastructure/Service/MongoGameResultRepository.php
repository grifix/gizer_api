<?php


namespace App\Infrastructure\Service;


use App\Application\Service\GameResultsRepository\Dto\GameResultDto;
use App\Application\Service\GameResultsRepository\Dto\UserDto;
use App\Application\Service\GameResultsRepository\Excepiton\InvalidSortingException;
use App\Application\Service\GameResultsRepository\GameResultsRepositoryInterface;
use App\Infrastructure\MongoDb\ConnectionInterface;

final class MongoGameResultRepository implements GameResultsRepositoryInterface
{
    private ConnectionInterface $connection;

    private const DB = 'db';
    private const COLLECTION = 'game_results';

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return \App\Application\Service\GameResultsRepository\Dto\GameResultDto[]
     *
     * @throws InvalidSortingException
     */
    public function getAll(?string $sortBy = null): array
    {
        if (null !== $sortBy && !in_array($sortBy, [self::SORT_BY_DATE, self::SORT_BY_SCORE])) {
            throw new InvalidSortingException($sortBy);
        }
        $result = [];
        $documents = $this->connection->find(self::DB, self::COLLECTION, null === $sortBy ? [] : [$sortBy => 1]);
        foreach ($documents as $document){
            $result[] = $this->restore($document);
        }
        return [];
    }

    /**
     * @param GameResultDto[] $gameResults
     */
    public function import(array $gameResults): void
    {
        $documents = [];
        foreach ($gameResults as $result) {
            $documents[] = $this->store($result);
        }
        $this->connection->startTransaction();
        $this->connection->truncateCollection(self::DB, self::COLLECTION);
        $this->connection->bulkWrite(self::DB, self::COLLECTION, $documents);
        $this->connection->commitTransaction();
    }

    private function store(GameResultDto $gameResultDto): array
    {
        return [
            'id' => $gameResultDto->getId(),
            'user' => [
                'id' => $gameResultDto->getUser()->getId(),
                'name' => $gameResultDto->getUser()->getName()
            ],
            'score' => $gameResultDto->getScore(),
            'finished_at' => $gameResultDto->getFinishedAt()
        ];
    }

    private function restore(array $document): GameResultDto
    {
        return new GameResultDto(
            $document['id'],
            new UserDto(
                $document['user']['id'],
                $document['user']['name']
            ),
            $document['score'],
            $document['finished_at']
        );
    }

}
