<?php


namespace App\Infrastructure\Service;


use App\Application\Service\GameResultClient\Dto\GameResultDto;
use App\Application\Service\GameResultClient\Dto\UserDto;
use App\Application\Service\GameResultsClient\GameResultClientInterface;

final class JacekMockGameResultsClient implements GameResultClientInterface
{
    public const URL = 'https://private-b5236a-jacek10.apiary-mock.com/results/games/1';


    public function getResults(): array
    {
        $result = [];
        $data = json_decode(file_get_contents(self::URL), true);
        foreach ($data as $record){
            $result[] = new GameResultDto(
                $record['id'],
                new UserDto(
                    $record['user']['id'],
                    $record['user']['name']
                ),
                $record['score'],
                $record['finished_at']
            );
        }
        return $result;
    }
}
