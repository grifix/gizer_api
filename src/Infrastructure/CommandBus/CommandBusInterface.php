<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandBus;

interface CommandBusInterface
{
    public function execute(object $command): void;
}
