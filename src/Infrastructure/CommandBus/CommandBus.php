<?php

declare(strict_types=1);


namespace App\Infrastructure\CommandBus;

use Symfony\Component\Messenger\MessageBusInterface;

final class CommandBus implements CommandBusInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }


    public function execute(object $command): void
    {
        $this->messageBus->dispatch($command);
    }
}
