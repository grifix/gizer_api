<?php

declare(strict_types=1);

namespace App\Ui\Http;

use App\Application\Query\GetGameResults\GetGameResultsQuery;
use App\Infrastructure\QueryBus\QueryBusInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class GetGameResultsRouteHandler
{
    private QueryBusInterface $queryBus;

    public function __construct(QueryBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }


    public function __invoke(Request $request): Response
    {
        $this->queryBus->execute(new GetGameResultsQuery());
        return new Response('ok');
    }
}
