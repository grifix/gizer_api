<?php


namespace App\Application\Query\GetGameResults;


final class GetGameResultsQuery
{
    private ?string $sortBy;

    public function __construct(?string $sortBy = null)
    {
        $this->sortBy = $sortBy;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }
}
