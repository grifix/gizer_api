<?php


namespace App\Application\Query\GetGameResults;


use App\Application\Service\GameResultsRepository\Dto\GameResultDto;
use App\Application\Service\GameResultsRepository\GameResultsRepositoryInterface;

final class GetGameResultsQueryHandler
{

    private GameResultsRepositoryInterface $gameResultsRepository;

    public function __construct(GameResultsRepositoryInterface $gameResultsRepository)
    {
        $this->gameResultsRepository = $gameResultsRepository;
    }

    /**
     * @return GameResultDto[]
     */
    public function __invoke(GetGameResultsQuery $query): array
    {
        return $this->gameResultsRepository->getAll($query->getSortBy());
    }
}
