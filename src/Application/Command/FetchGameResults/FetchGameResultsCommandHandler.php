<?php


namespace App\Application\Command\FetchGameResults;


use App\Application\Service\GameResultsClient\GameResultClientInterface;
use App\Application\Service\GameResultsRepository\Dto\GameResultDto;
use App\Application\Service\GameResultsRepository\Dto\UserDto;
use App\Application\Service\GameResultsRepository\GameResultsRepositoryInterface;

final class FetchGameResultsCommandHandler
{
    private GameResultClientInterface $client;

    private GameResultsRepositoryInterface $repository;

    /**
     * FetchGameResultsCommandHandler constructor.
     * @param GameResultClientInterface $client
     * @param GameResultsRepositoryInterface $repository
     */
    public function __construct(GameResultClientInterface $client, GameResultsRepositoryInterface $repository)
    {
        $this->client = $client;
        $this->repository = $repository;
    }


    public function __invoke(FetchGameResultsCommand $command): void
    {
        $results = $this->client->getResults();
        $documents = [];
        foreach ($results as $result){
            $documents[] = new GameResultDto(
                $result->getId(),
                new UserDto(
                    $result->getUser()->getId(),
                    $result->getUser()->getName()
                ),
                $result->getScore(),
                $result->getFinishedAt()
            );
        }
        $this->repository->import($documents);
    }
}
