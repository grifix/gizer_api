<?php


namespace App\Application\Service\GameResultsRepository;


use App\Application\Service\GameResultsRepository\Dto\GameResultDto;
use App\Application\Service\GameResultsRepository\Excepiton\InvalidSortingException;
use App\Infrastructure\Service\MongoGameResultRepository;

interface GameResultsRepositoryInterface
{

    public const SORT_BY_DATE = 'date';
    public const SORT_BY_SCORE = 'score';

    /**
     * @return GameResultDto[]
     *
     * @throws InvalidSortingException
     */
    public function getAll(?string $sortBy = null): array;

    /**
     * @param GameResultDto[] $gameResults
     */
    public function import(array $gameResults): void;
}
