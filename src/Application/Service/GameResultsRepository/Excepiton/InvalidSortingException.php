<?php


namespace App\Application\Service\GameResultsRepository\Excepiton;


use Throwable;

final class InvalidSortingException extends \InvalidArgumentException
{
    public function __construct(string $sorting)
    {
        parent::__construct(sprintf('Invalid sorting "%s"!', $sorting));
    }
}
