<?php


namespace App\Application\Service\GameResultsClient;


use App\Application\Service\GameResultClient\Dto\GameResultDto;

interface GameResultClientInterface
{
    /**
     * @return GameResultDto[]
     */
    public function getResults():array;
}
