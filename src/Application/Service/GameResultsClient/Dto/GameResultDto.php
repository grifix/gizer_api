<?php


namespace App\Application\Service\GameResultClient\Dto;

final class GameResultDto
{
    private string $id;

    private UserDto $user;

    private int $score;

    private string $finishedAt;

    public function __construct(string $id, UserDto $user, int $score, string $finishedAt)
    {
        $this->id = $id;
        $this->user = $user;
        $this->score = $score;
        $this->finishedAt = $finishedAt;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUser(): UserDto
    {
        return $this->user;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function getFinishedAt(): string
    {
        return $this->finishedAt;
    }

}
